<!doctype html>
<html lang="en-US" prefix="og: http://ogp.me/ns#" class="no-js">
<head>
	<meta charset="UTF-8">
	<title>Movie Bohx - Manage Your Movies With Ease</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="This is a demo MVP app by David Brooks." />
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
	<link rel="stylesheet" href="/_css/master.css" type="text/css" />
</head>
<body class="home">
	<div class="wrapper">
		<div class="placeholder">
			<h1>Welcome to Movie Bohx</h1>
			<p>We'll have something to show you soon. Until then, what's your favorite movie?</p>
		</div><!-- /.placeholder -->
	</div> <!-- /.wrapper -->
	<script src="/_js/moviebohx.min.js"></script>
</body>
</html>