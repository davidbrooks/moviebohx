<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', function (Request $request, Response $response, array $args) {
	// This is the root path, but they shouldn't really get here without authing
	$auth = authRequired();
	if ($auth == 0) {
		// Auth wasn't met, so we send them to /sign-in
		return $response->withRedirect('/sign-in/'); 
	} else {
		// They have an API key and ID, so we render the index page
		return $this->view->render($response, 'index.html', [
			'active_page' => 'home'
		]);
	}
});

$app->get('/sign-in/', function (Request $request, Response $response, array $args) {
	// This is the route where we show them the auth screen
	// We check here for errors in a previous auth attempt and pass them off to the screen
	if (isset($_SESSION['auth_errors'])) {
		$auth_errors = $_SESSION['auth_errors'];
	} else {
		$auth_errrors = array();
	}
	return $this->view->render($response, 'sign-in.html', [
		'auth_errors' => $auth_errors
	]);
});

$app->get('/sign-out/', function (Request $request, Response $response, array $args) {
	// Here we unset the session and pass them back to the /sign-in route
	session_unset();
	return $response->withRedirect('/sign-in/'); 
	//return $this->view->render($response, 'sign-in.html', []);
});

$app->get('/sign-up/', function (Request $request, Response $response, array $args) {
	// This is the sign-up interface, we also check for errors from previous auth attempts
	if (isset($_SESSION['auth_errors'])) {
		$auth_errors = $_SESSION['auth_errors'];
	} else {
		$auth_errrors = array();
	}
	return $this->view->render($response, 'sign-up.html', [
		'auth_errors' => $auth_errors
	]);
});

$app->post('/sign-in/', function (Request $request, Response $response, array $args) {
	// This processes the sign-in attempt
	$results = array();
	$errors = array();
	// This clears the errors list so that we can see what happens with this auth attempt
	$_SESSION['auth_errors'] = array();
	// These are the values passed from the user
	// If there's something wrong with it, we pass that back in an error message
	if (isset($request->getParsedBody()['email_address'])) {
		$email_address = sanitize($request->getParsedBody()['email_address']);
	} else {
		$email_address = '';
		array_push($errors, 'An email address is required.');
	}
	if (isset($request->getParsedBody()['password'])) {
		$password = sanitize($request->getParsedBody()['password']);
		$password = sha1($password);
	} else {
		array_push($errors, 'A password is required.');
	}
	// At this point, we check for errors
	if (count($errors) == 0) {
		// If we're good, we're good, we query the database to find out if their credentials match
		foreach($this->db->query("SELECT uid, apikey FROM users WHERE email_address = '$email_address' AND password = '$password'") as $row) {
			// We then stash this into the session 
		    $_SESSION['apikey'] = $row['apikey'];
		    $_SESSION['uid'] = $row['uid'];
		}
		// This is the actual check to see if something was set.
		// If there's not a user id (uid) set in the session, we didn't find them in the previous query
		if (!isset($_SESSION['uid'])) {
			array_push($errors, 'Email or password was not correct.');
			$_SESSION['auth_errors'] = $errors;
		}
		// One way or another, we kick them back to /. It will redirect them if their credentials aren't correct
		return $response->withRedirect('/'); 
	} else {
		// We know there were problems, so we don't even check the email + password. 
		// We just redirect them back to correct.
		$_SESSION['auth_errors'] = $errors;
		return $response->withRedirect('/sign-in/'); 
	}
    return json_encode($results);
});

$app->post('/sign-up/', function (Request $request, Response $response, array $args) {
	// This is the fielding of the sign-up form
	$results = array();
	$errors = array();
	// Reset the auth errors
	$_SESSION['auth_errors'] = array();
	// Verify all of the required data is present in the form
	if (isset($request->getParsedBody()['email_address'])) {
		$email_address = sanitize($request->getParsedBody()['email_address']);
	} else {
		array_push($errors, 'An email address is required.');
	}
	if (isset($request->getParsedBody()['fullname'])) {
		$fullname = sanitize($request->getParsedBody()['fullname']);
	} else {
		$fullname = '';
	}
	if (isset($request->getParsedBody()['password'])) {
		$password = sanitize($request->getParsedBody()['password']);
		$password = sha1($password);
	} else {
		array_push($errors, 'A password is required.');
	}
	// We need to make sure they don't have an account already
	$user_count = 0;
	foreach($this->db->query("SELECT uid FROM users WHERE email_address = '$email_address'") as $row) {
	    // So, here the email address is already in use
	    $user_count = $user_count + 1;
	}
	if ($user_count != 0) {
		// If this condition is met, they already have an account
		array_push($errors, 'That email address is already in use.');
	}
	if (count($errors) == 0) {
		// Here we don't have any errors, so we sign them up
		$apikey = makeAPIKey($email);
		$query = $this->db->query("INSERT INTO users (fullname, email_address, password, apikey) VALUES ('$fullname', '$email_address', '$password', '$apikey')"); 
		// We then turn around and re-request their uid and apikey to set in the session
		// That means we don't have to redirect them to sign in.
		// If we were going to force verification by email, this is where we could drop that piece in
		foreach($this->db->query("SELECT uid, apikey FROM users WHERE email_address = '$email' AND password = '$password'") as $row) {
	    	$_SESSION['apikey'] = $row->apikey;
	    	$_SESSION['uid'] = $row->uid;
		}
		return $response->withRedirect('/');
	} else {
		// Mistakes were made. We send them back to try again
		$_SESSION['auth_errors'] = $errors;
		return $response->withRedirect('/sign-up/'); 
	}
});

// All of the routes relating to the movie collection are here
$app->group('/movies', function () use ($app) {
	$app->post('/add/', function (Request $request, Response $response, array $args) {
		// This route is for processing their movie addition request
		$results = array();
		// This is their user id. We use it to match their movie to them
		$uid = sanitize($_SESSION['uid']);
		// These values were shipped over from the UI, most are directly from the OMDB API
		if (isset($request->getParsedBody()['movie_title'])) {
			$movie_title = sanitize($request->getParsedBody()['movie_title']);
		} else {
			// Movie titles should not be blank, ever
			// "Unknown" is used here, but we could set this to anything we need
			// We could even pass over the value from OMDB again to force the real title
			$movie_title = 'Unknown';
		}
		if (isset($request->getParsedBody()['movie_format'])) {
			// This can be either DVD, VHS, or Streaming
			$movie_format = sanitize($request->getParsedBody()['movie_format']);
		} else {
			$movie_format = '';
		}
		if (isset($request->getParsedBody()['movie_rating'])) {
			// Values are from 1 to 5, if you don't pass one over, we assume it's a 1.
			$movie_rating = sanitize($request->getParsedBody()['movie_rating']);
			if ($movie_rating < 1) { $movie_rating = 1; }
			if ($movie_rating > 5) { $movie_rating = 5; }
		} else {
			$movie_rating = 1;
		}
		if (isset($request->getParsedBody()['movie_release'])) {
			// This is the year the movie was released
			$movie_release = sanitize($request->getParsedBody()['movie_release']);
		} else {
			$movie_release = '';
		}
		if (isset($request->getParsedBody()['movie_length'])) {
			// This is how many minutes long the movie is
			$movie_length = (int)sanitize($request->getParsedBody()['movie_length']);
		} else {
			$movie_length = '';
		}
		if (isset($request->getParsedBody()['imdbID'])) {
			// OMDB uses this value ad an ID, so we stash it here and use it to look up the movie art
			$imdbID = sanitize($request->getParsedBody()['imdbID']);
		} else {
			$imdbID = '';
		}
		// This is the call over to add the movie. 
		// We could put a check in place for empty values or whatever, but we have enough default values to satisfy criteria 
		$query = $this->db->query("INSERT INTO movies (uid, movie_title, movie_format, movie_rating, movie_length, movie_release, imdbID) VALUES ('$uid', '$movie_title', '$movie_format', '$movie_rating', '$movie_length', '$movie_release', '$imdbID')"); 
	    return $response->withRedirect('/movies/'); 
	});
	$app->post('/delete/{mid}/', function (Request $request, Response $response, array $args) {
		// This is the route used to delete a movie from the collection
		$mid = sanitize($args['mid']);
		$uid = sanitize($_SESSION['uid']);
		$query = $this->db->query("DELETE FROM movies WHERE mid = '$mid' AND uid = '$uid'");
		return $response->withRedirect('/movies/'); 
	});
	$app->get('/{mid}', function (Request $request, Response $response, array $args) {
		// This route hands back the original OMDB data and the user's customization of that data
		// mid is the movie id, this route is based on that, not the OMDB ID
		$mid = sanitize($args['mid']);
		$apikey = $this->get('settings')['omdbapi'];
		$uid = sanitize($_SESSION['uid']);
		$movie_customizations = '';
		$imdbID = '';
		$results = '';
		// First we find all of the movie information they have saved
		foreach($this->db->query("SELECT * FROM movies WHERE uid = '$uid' AND mid = '$mid'") as $row) {
			// We set this over to the customizations variable for display
	    	$movie_customizations = $row;
	    	$imdbID = $row['imdbID'];
	    }
	    if ($imdbID != '') {
	    	// Here we check for the IMDB ID and query out to the OMDB API
			$url = 'http://www.omdbapi.com/?apikey='.$apikey.'&i='.$imdbID;
			$results = json_decode(getResource($url));
	    }
	    // Here we merge the two sets of info and render the UI
	    return $this->view->render($response, 'update.html', [
	    	'movie_customizations' => $movie_customizations,
	    	'movie_details' => $results,
	    	'active_page' => 'movies'
	    ]);
	});
	$app->post('/{mid}', function (Request $request, Response $response, array $args) {
		// This is the route for updating the movie instance
		$mid = sanitize($args['mid']);
		$uid = sanitize($_SESSION['uid']);
		if (isset($request->getParsedBody()['movie_title'])) {
			$movie_title = sanitize($request->getParsedBody()['movie_title']);
		} else {
			$movie_title = 'Unknown';
		}
		if (isset($request->getParsedBody()['movie_format'])) {
			$movie_format = sanitize($request->getParsedBody()['movie_format']);
		} else {
			$movie_format = '';
		}
		if (isset($request->getParsedBody()['movie_rating'])) {
			// Movie ratings must be between 1 and 5
			$movie_rating = sanitize($request->getParsedBody()['movie_rating']);
			if ($movie_rating < 1) { $movie_rating = 1; }
			if ($movie_rating > 5) { $movie_rating = 5; }
		} else {
			$movie_rating = 1;
		}
		// This is the actual query for updating the movie information
		$query = $this->db->query("UPDATE movies SET movie_title = '$movie_title', movie_format = '$movie_format', movie_rating = '$movie_rating' WHERE mid = '$mid' AND uid = '$uid'");
		return $response->withRedirect('/movies/'); 
	});
	
	$app->get('/', function (Request $request, Response $response, array $args) {
		// This provides a list for all movies in a user's collection
	    $movies = array();
	    $uid = sanitize($_SESSION['uid']);
	    $criteria = 'movie_title';
	    // We assume they care about the movie title by default. 
	    // If they've sent over another value we use that instead for the query
	    if ($request->getParam('filter_by')) {
	    	$criteria = sanitize($request->getParam('filter_by'));
	    }
	    foreach($this->db->query("SELECT * FROM movies WHERE uid = '$uid' ORDER BY $criteria") as $row) {
	    	array_push($movies, $row);
	    }
	    return $this->view->render($response, 'movies.html', [
	        'movie_list' => $movies,
	        'filter_by' => $criteria,
	        'active_page' => 'movies'
	    ]);
	});
});

// This is the collection of routes around the search functionality
$app->group('/search', function () use ($app) {
	$app->get('/{imdbID}', function (Request $request, Response $response, array $args) {
		// This is used to look up a single movie in the OMDB catalog
		$imdbID = sanitize($args['imdbID']);
		$apikey = $this->get('settings')['omdbapi'];
		$url = 'http://www.omdbapi.com/?apikey='.$apikey.'&i='.$imdbID;
		$results = json_decode(getResource($url));
	    return $this->view->render($response, 'result.html', [
	    	'movie_details' => $results,
	    	'active_page' => 'search'
	    ]);

	});
	$app->get('/', function (Request $request, Response $response, array $args) {
		// This is the actual URL the user will see
	    return $this->view->render($response, 'search.html', [
	    	'active_page' => 'search'
	    ]);
	});
	$app->post('/', function (Request $request, Response $response, array $args) {
		// This is called via an "AJAX" request from the browser. 
		// It queries OMDB and compares it with the user's movie data
		$results = array();
		$my_movies = array();
		$apikey = $this->get('settings')['omdbapi'];
		$uid = sanitize($_SESSION['uid']);
		// This is where we grab their movie collection data
	    foreach($this->db->query("SELECT * FROM movies WHERE uid = '$uid'") as $row) {
	    	array_push($my_movies, $row['imdbID']);
	    }
		if (isset($request->getParsedBody()['movie_title'])) {
			// We then get the results from OMDB
			$movie_title = urlencode(sanitize($request->getParsedBody()['movie_title']));
			$url = 'http://www.omdbapi.com/?apikey='.$apikey.'&s='.$movie_title;
			$results = json_decode(getResource($url));
			$movie_results = $results->Search;
			foreach($movie_results as $movie) {
				// This looks to see if the movie is already in their collection
				if (in_array($movie->imdbID, $my_movies)) {
					$movie->Mine = 'mine';
				} else {
					$movie->Mine = '';
				}
			}
		} 
	    return json_encode($results);
	});
});

function authRequired() {
	// This is super simple. It just checks if you have an api key and user id in session
	if (isset($_SESSION['apikey']) && isset($_SESSION['uid'])) {
		return 1;
	} else {
		return 0;
	}
}

function getResource($url) {
	// This is used for getting remote resources, in this case mostly OMDB assets
	$curl = curl_init();
	curl_setopt ($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	curl_close ($curl);
	return $result;
}

function makeAPIKey($base) {
	// This is a centralized function for creating consistant, non-repeat API keys
	$apikey = $base.''.date("Y-m-d H:i:s");
	return sha1($apikey);
}

function sanitize($input) {
	// This function is used for clearing user input of anything iffy
	$output = addSlashes(htmlentities(strip_tags($input)));
	return $output;
}