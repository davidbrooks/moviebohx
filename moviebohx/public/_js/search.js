var search = (function (window, document, utilities, Handlebars, undefined) {
	return {
		goSearch: function() {
			// This looks up the current value of the search field
			var searchTerm = 'movie_title=' + document.getElementById("search-field").value;
			if (searchTerm && searchTerm !== '' && searchTerm !== ' ') {
				utilities.post('/search/', searchTerm)
					.then(handleSuccess)
					.catch(handleFailure);
			}
			function handleFailure(response) {
				console.log(JSON.parse(response));
			}
			function handleSuccess(response) {
				document.getElementById("search-results").innerHTML = '';
				var data = JSON.parse(response);
				var source = document.getElementById("movie-list").innerHTML;
				var template = Handlebars.compile(source);
				var context = {
					results: data.Search,
					results_count: parseInt(data.totalResults)
				}
				var html = template(context);
				document.getElementById("search-results").innerHTML = html;
				
			}
		}
	};
})(this, this.document, utilities, Handlebars);
