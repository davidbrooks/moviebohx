var utilities = (function (window, document, undefined) {
	return {
		closeModal: function(coverId) {
			// This is a generic modal closer. You supply it with an ID and it removes the classes
			if (document.getElementById(coverId)) {
				document.getElementById(coverId).classList.remove('active');
			}
		},
		debounce: function(func, delay) {
			// For this function, I borrowed from https://codeburst.io/throttling-and-debouncing-in-javascript-b01cad5c8edf
			var inDebounce;
			return function() {
    			var context = this;
    			var args = arguments;
			    clearTimeout(inDebounce);
    			inDebounce = setTimeout(function() {
    				func.apply(context, args)
    			}, delay);
    		}
		},
		openModal: function(coverId) {
			// This is a generic modal opener. You pass an ID to it, and it adds the classes to toggle it open
			if (document.getElementById(coverId)) {
				document.getElementById(coverId).classList.toggle('active');
			}
		},
		post: function(url, data) {
			var promise = new Promise(function(resolve, reject) {
				var http = new XMLHttpRequest();
				http.open("POST", url, true);
				http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				http.onreadystatechange = function() { 
					if (http.readyState === 4 && http.Response !== 'False') {
						if (http.status === 200) {
							resolve(http.response);
						} else {
							reject(Error(http.response));
						}
					}
				}
				http.send(data);
			});
			return promise;
		}
	};
})(this, this.document);
