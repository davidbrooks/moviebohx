var interaction = (function (window, document, utilities, search, Handlebars, undefined) {
	return {
		doNotDelete: function(event) {
			// This is another pass-through function. It calls for the closing of the movie deletion modal
			utilities.closeModal('question-delete');
			event.preventDefault();
		},
		goFilter: function() {
			var filter_by = document.getElementById("filter-by").value;
			window.location.href = "/movies/?filter_by=" + filter_by;
		},
		questionDelete: function(event) {
			// This is mostly a pass-through function. It calls the opening of the movie deletion modal and stops the event
			utilities.openModal('question-delete');
			event.preventDefault();
		},
		setupClicks: function() {
			// This function attaches listeners to various points throughout the app.
			document.getElementById("navigation-toggle").addEventListener("click", interaction.toggleNavigation);
			if (document.getElementById("delete-movie")) {
				document.getElementById("delete-movie").addEventListener("click", interaction.questionDelete);
			}
			// This is a button in the movie deletion modal. When clicked, it just toggles the closing of the modal
			if (document.getElementById("close-delete-question")) {
				document.getElementById("close-delete-question").addEventListener("click", interaction.doNotDelete);
			}
		},
		setupFilters: function() {
			if (document.getElementById("filter-by")) {
				document.getElementById("filter-by").addEventListener("change", interaction.goFilter);
			}
		},
		setupPrefs: function() {
			// Some features may need to persist between page loads, this is where we check for those
			// Standard prefix is "mb"
			// "Feature" indicator follows the :
			// First, check if they have a value for navigation
			if (localStorage.getItem("mb:nav")) {
				// See if they last closed it, otherwise no action is required
				if (localStorage.getItem("mb:nav") == "closed") {
					// The default state is open, so here we close the nav
					interaction.toggleNavigation();
				}
			} else {
				// In this case there is no existing value for the navigation, so we set the default out of self-defense
				localStorage.setItem("mb:nav", "open");
			}
		},
		setupRating: function() {
			if (document.getElementById("inline-rating")) {
				// First we find the rating in the UI, here it's done by ID, but we could easily convert this to be class-based
				var ratingUI = document.getElementById("inline-rating");
				//  We clear the content here, incase we're re-drawing it
				ratingUI.innerHTML = '';
				// This is the actual input we need to adjust, the data from it is passed on to the server
				var target =  ratingUI.dataset.target;
				// And this is the current value of the real input
				var currentRating = document.getElementById(target).value;
				// Right now this is set to 5, it could be adjusted to 3 or 10 if needed
				var maxRating = 5;
				// This is the difference between the max end of the scale and what they have selected at the moment
				var ratingDifference = maxRating - currentRating;
				// These are the two templates in use for the star ratings
				var emptySource = document.getElementById("inline-empty-stars").innerHTML;
				var filledSource = document.getElementById("inline-filled-stars").innerHTML;
				// We then roll through until the max rating is reached, placing filled or empty stars accordingly
				for(var i = 0; i < maxRating; ++i) {
					if (i < currentRating) {
						var template = Handlebars.compile(filledSource);
					} else {
						template = Handlebars.compile(emptySource);
					}
					// We need to pass the current iterator and the target input to the UI
					// It can be called with interaction.updateRating(value, target);
					var context = {
						data_value: i,
						data_target: target,
					};
					// This just mashes the chosen star against the data from context
					var html = template(context);
					// ...and then it appends it to the UI
					ratingUI.insertAdjacentHTML('beforeend', html);
				}
			}
		},
		setupSearch: function() {
			// This assigns an event listener to the search field
			if (document.getElementById("search-field")) {
				document.getElementById("search-field").addEventListener("keydown", utilities.debounce(function() {
					search.goSearch();
				}, 250));
			}
		},
		// interaction.toggleNavigation();
		toggleNavigation: function(event) {
			// First we find the main-wrapper element. It holds overarching CSS class data
			// Then we toggle the class for "navigation-closed"
			var mainWrapper = document.querySelector('#main-wrapper');
			mainWrapper.classList.toggle('navigation-open');
			// This is called by user action AND by the preferences. This determines why this instance was called
			if (event) {
				// Since there's an event, a person likely clicked the button
				// So, we find their current setting and do the opposite.
				if (localStorage.getItem("mb:nav") === "open") {
					localStorage.setItem("mb:nav", "closed");
				} else {
					localStorage.setItem("mb:nav", "open");
				}
				event.preventDefault();
			}
		},
		updateRating: function(value, target) {
			// This function is directly called from the UI. 
			// It passes a value and a rating target to be updated
			// Rating values are handed over as 0 to 4, +1 is added here to offset
			var newValue = parseInt(value) + 1;
			document.getElementById(target).value = newValue;
			// This recalls the rendering of the UI so that the updated value will show
			interaction.setupRating();
		}
	};
})(this, this.document, utilities, search, Handlebars);

(function() {
	// When the page loads, we call a few functions to get things started
	interaction.setupClicks();
	interaction.setupFilters();
	interaction.setupPrefs();
	interaction.setupSearch();
	interaction.setupRating();
})();