module.exports = function(grunt) {
  'use strict';
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['node_modules/handlebars/dist/handlebars.js', '_js/utilities.js', '_js/search.js', '_js/interaction.js'],
        dest: '_js/moviebohx.min.js'
      }
    },
    sass: {
      dist: {
        files: {
          '_css/master.css': '_sass/master.scss'
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          '_js/moviebohx.min.js': ['_js/moviebohx.min.js']
        }
      }
    },
    watch: {
      files: ['_sass/*.scss', '_js/interaction.js', '_js/search.js', '_js/utilities.js'],
      tasks: ['sass', 'concat', 'uglify']
    }
  });
  grunt.loadNpmTasks('grunt-bower-concat');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.registerTask('default', ['sass','concat', 'uglify']);

};
