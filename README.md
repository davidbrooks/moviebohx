Setting Up Movie Bohx
==============

MovieBohx uses the PHP framework Slim (v3), so you'll need a PHP 7.1. 

In the moviebohx/src directory you'll find a config-example.php file. Copy that as config.php and update it with the correct values.

Production Environment
--------------

From the moviebohx directory run:

composer install --no-dev

The project itself is served from moviebohx/public. 
So just point your MAMP/LAMP/Apache2 configuration at that directory and you're good to go.

Development
--------------

To setup the Slim framework go to the moviebohx directory and run:

composer install 

To make updates to the front-end visit moviebohx/public and run:

npm install
grunt watch

You will still need to point MAMP/LAMP/Apache2 at the moviebohx/public directory.

Develop away!